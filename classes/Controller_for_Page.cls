public class Controller_for_Page {
    
    //get Product
    public List<Storage__c> listProduct {get; set;} 
    public List<Storage__c> listWithCountFromBase {get; set;}
    public String startStringQuery {get; set;}
    
    //delete
    public Boolean displayPopupDel {get; set;}
    public Id idForDel {get; set;}
    public String StringForDel {get; set;}
    
    //sorting
    public String sortField {get; set;}
    public String orderDirection {get; set;}
    
    //search
    public String stringWithInputFromPage {get; set;}
    public Date dateForSearchStart {get; set;}
    public Date dateForSearchEnd {get; set;}
    
    //pagination
    public Integer total_pages {get; set;}
    public Integer counter {get; set;} 
    public Integer list_size = 10; 
    public Integer total_records;
    public Integer pages {get; set;}
    
    //Add Product
    public Storage__c storage {get; set;}
    public Boolean displayAddPopup {get; set;}
    
    //edit Product
    public Boolean displayEditPopup {get; set;}
    public Id IdForEdit {get; set;}
    
    public Controller_for_Page() {
        startStringQuery = ' SELECT Id, Name, Cost__c, Amount__c, Type__c, Date_Added__c, Date_Release__c, Availability__c FROM Storage__c';
        orderDirection = 'ASC';
        storage = new Storage__c();
        pages = 1;
        counter = 0;
        getProductFromBase();
    }
    
    //pagination
    public PageReference Previous() {
        pages --; 
        counter --;
        getProductFromBase(); 
        return null;
    }
    
    public Boolean getDisablePrevious() {
        return (counter>0) ?  false :  true;      
    }
    
    public PageReference Next() {
        pages ++;
        counter ++;      
        getProductFromBase();
        return null;
    }
    
    public Integer getTotalPages() {
        if (math.mod(total_records, list_size) > 0) {
            return  total_records/list_size + 1;
        } else {
            return ( total_records/list_size);
        }
    }
    
    public Boolean getDisableNext() {
        return (pages == getTotalPages() || pages == 0) ?  true :  false;
    }
    
    public void SortTable() {
        orderDirection = orderDirection == 'ASC' ? 'DESC' : 'ASC';
        getProductFromBase();
    }
    
    public void  getProductFromBase() {
        String sortQuery = (sortField != null) ? ' ORDER BY ' + sortField + ' ' + orderDirection : '';//
        String stringWithConditions = ' WHERE Name Like \'' + stringWithInputFromPage + '%\'';
        String stringWithOffSet = ' LIMIT ' + list_size + ' OFFSET ' + list_size * counter;
        List<Storage__c> listWithCountFromBase = new List<Storage__c>();
        String queryAllStringsWithountOffset;
        
        String queryAllString; 
        // if all conditions 
        if( (dateForSearchStart != null) && (dateForSearchEnd != null) && ( String.isNotBlank(stringWithInputFromPage)) ) {
            Datetime a = Datetime.newInstance(dateForSearchStart.year(), dateForSearchStart.month(), dateForSearchStart.day());
            Datetime b = Datetime.newInstance(dateForSearchEnd.year(), dateForSearchEnd.month(), dateForSearchEnd.day());        
            stringWithConditions = ' WHERE Name Like \'' + stringWithInputFromPage + '%\' AND  Date_Added__c >= ' + a.format('yyyy-MM-dd') + ' AND Date_Added__c <= ' + b.format('yyyy-MM-dd');            
            queryAllString = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery + ' ' + stringWithOffSet ; 
            queryAllStringsWithountOffset = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery;
            // date yes, string no    
        } else if( (dateForSearchStart != null) && (dateForSearchEnd != null) && ( String.isBlank(stringWithInputFromPage)) ) { 
            Datetime a = Datetime.newInstance(dateForSearchStart.year(), dateForSearchStart.month(), dateForSearchStart.day());
            Datetime b = Datetime.newInstance(dateForSearchEnd.year(), dateForSearchEnd.month(), dateForSearchEnd.day());  
            stringWithConditions = ' WHERE Date_Added__c >= ' + a.format('yyyy-MM-dd') + ' AND Date_Added__c <= ' + b.format('yyyy-MM-dd');
            queryAllString = startStringQuery + stringWithConditions + ' ' + sortQuery + ' ' + stringWithOffSet;
            queryAllStringsWithountOffset = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery;	
            // 1 date yes, string no   
        } else if( (dateForSearchStart == null) && (dateForSearchEnd != null) && ( String.isBlank(stringWithInputFromPage)) ) { 
            Datetime b = Datetime.newInstance(dateForSearchEnd.year(), dateForSearchEnd.month(), dateForSearchEnd.day());  
            stringWithConditions = ' WHERE Date_Added__c <= ' + b.format('yyyy-MM-dd');
            queryAllString = startStringQuery + stringWithConditions + ' ' + sortQuery + ' ' + stringWithOffSet;
            queryAllStringsWithountOffset = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery;
            // 1 date yes, string no   
        } else if( (dateForSearchStart != null) && (dateForSearchEnd == null) && ( String.isBlank(stringWithInputFromPage)) ) { 
            dateForSearchEnd = Date.today();
            Datetime a = Datetime.newInstance(dateForSearchStart.year(), dateForSearchStart.month(), dateForSearchStart.day());
            Datetime b = Datetime.newInstance(dateForSearchEnd.year(), dateForSearchEnd.month(), dateForSearchEnd.day());  
            stringWithConditions = ' WHERE Date_Added__c >= ' + a.format('yyyy-MM-dd') + ' AND Date_Added__c <= ' + b.format('yyyy-MM-dd');
            queryAllString = startStringQuery + stringWithConditions + ' ' + sortQuery + ' ' + stringWithOffSet;
            queryAllStringsWithountOffset = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery;
            // date no, string yes    
        } else if( (dateForSearchStart == null) && (dateForSearchEnd == null) && ( String.isNotBlank(stringWithInputFromPage)) ) {
            queryAllString = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery + ' ' + stringWithOffSet;
            queryAllStringsWithountOffset = startStringQuery + ' ' + stringWithConditions + ' ' + sortQuery;
            // no conditions   
        } else { 
            queryAllString = startStringQuery + ' ' + sortQuery + ' ' + stringWithOffSet;      
            queryAllStringsWithountOffset = startStringQuery + ' ' + sortQuery;
        }
        listProduct = Database.query(queryAllString);
        listWithCountFromBase = Database.query(queryAllStringsWithountOffset);
        total_records = listWithCountFromBase.size();
        getTotalPages();
    }  
    
    //edit popap 
    public void closeEditPopup() {        
        displayEditPopup = false;    
    } 
    
    public void showEditPopup() { 
        System.debug('IdForEdit' + IdForEdit);
        storage = [SELECT Id, Name, Cost__c, Amount__c, Type__c, Date_Added__c, Date_Release__c, Availability__c FROM Storage__c WHERE Id =: IdForEdit];
        displayEditPopup = true;    
    }
    
    public void editProduct() {
        closeEditPopup();        
        try {
            update storage;
            getProductFromBase();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Product was edited succesfuly!'));
        } catch(dmlexception e) {apexpages.addmessages(e); } 
    }
    
    //add popap
    public void closeAddPopup() {        
        displayAddPopup = false;    
    } 
    
    public void showAddPopup() {        
        displayAddPopup = true;    
    }
    
    public void addProduct() {
        closeAddPopup();
        try {
            insert storage; 
            storage = new Storage__c();
            getProductFromBase();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Product was created succesfuly!'));
        } catch(dmlexception e) {apexpages.addmessages(e); } 
    }
    
    // del popap
    public void closeDelPopup() {        
        displayPopupDel = false;    
    } 
    
    public void showDelPopup() {  
        displayPopupDel = true;    
    }
    
    public void deleteProduct() {
        displayPopupDel = false; 
        try {
            delete [SELECT Id
                    FROM Storage__c
                    WHERE Id =:IdForDel];
            getProductFromBase();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Product was delete succesfuly!'));
        } catch(dmlexception e) { apexpages.addmessages(e); }    
    }
}