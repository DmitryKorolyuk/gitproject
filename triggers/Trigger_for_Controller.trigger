trigger Trigger_for_Controller on Storage__c (before insert, before update) {
    if( (trigger.isInsert  && trigger.isBefore) || (trigger.isUpdate && trigger.isBefore) ) {
        TriggerHandler.methodForBeforeInsert(trigger.new);
    }
}