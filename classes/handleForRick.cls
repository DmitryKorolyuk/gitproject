/**
 * Created by User on 27.02.2019.
 */

public with sharing class handleForRick {

    //easy

    public static void addRickForMorty(List<Contact> listRick) {
        List<Contact> mainListContacts = new List<Contact>();
        for(Contact item : listRick) {
            if(item.FirstName=='Rick' && item.LastName=='Sanchez') {
                Contact contactMorty = new Contact(LastName='Smith', FirstName='Morty');
                mainListContacts.add(contactMorty);
            }
        }
        insert mainListContacts;
    }


    //medium
    /*
    public static void addIDRickForMorty(List<Contact> listRick) {
        List<Contact> mainListContacts = new List<Contact>();
        for(Contact item : listRick) {
            if(item.FirstName=='Rick' && item.LastName=='Sanchez') {
                Contact contactMorty = new Contact(LastName='Smith', FirstName='Morty', MyRick__c=item.id);
                mainListContacts.add(contactMorty);
            }
        }
        insert mainListContacts;
    }
    public static void checkBox(List<Contact> listMorty) {
        for(Contact item : listMorty) {
            if(item.FirstName=='Morty' && item.LastName=='Smith' && item.MyRick__c==null) {
                item.SadMorty__c=true;
            } else {
                item.SadMorty__c=false;
            }
        }
    }
    */

    //hard
    /*
    public static void checkBox(List<Contact> listMorty) {
        for(Contact item : listMorty) {
            if(item.FirstName=='Morty' && item.LastName=='Smith' && item.MyRick__c==null) {
                item.SadMorty__c=true;
            } else {
                item.SadMorty__c=false;
            }
        }
    }
    public static void addRickToMorty(List<Contact> lisContacts) {
        System.debug('incoming count contacts = ' + lisContacts.size());
        List<Contact> mainList = new List<Contact>();
        List<Contact> listMorty = new List<Contact>();
        List<Contact> listRick = new List<Contact>();
        for(Contact item : lisContacts) {
            if(item.FirstName=='Morty') {
                listMorty.add(item);
            } else {
                listRick.add(item);
            }
        }
        System.debug('how many Morty = ' + listMorty.size());
        System.debug('how many Rick = ' + listRick.size());
        System.debug('how many coupe = ' + mainList.size());
        List<Id> listIdRicks = new List<Id>();

        completeCoupe(lisContacts);     //another method fo update records
    }

    public static void completeCoupe(List<Contact> lisContacts) {       //put list from trigger, when create 200
        List<Contact> mainList = new List<Contact>();
        List<Contact> listMorty = new List<Contact>();
        List<Id> listIdRicks = new List<Id>();      //get Id in Rick
        for(Contact item : lisContacts) {
            if(item.FirstName=='Rick') {
                listIdRicks.add(item.Id);       //list with Id Rick
            } else {
                listMorty.add(item);        //list with Morty
            }
        }
        List<Id> idByMorty = new List<Id>();        //list with id Morty for base query
        for(Contact item : listMorty) {
            idByMorty.add(item.Id);
        }

        List<Contact> backListInBase = [SELECT FirstName, Id, MyRick__c FROM Contact WHERE Id IN : idByMorty];

        if(listMorty.size() > listIdRicks.size()) {
            for(Integer i=0; i<listIdRicks.size(); i++) {
                backListInBase[i].MyRick__c = listIdRicks[i];
                mainList.add(backListInBase[i]);
            }
        } else if(listMorty.size() < listIdRicks.size()) {
            for(Integer i=0; i<listMorty.size(); i++) {
                backListInBase[i].MyRick__c = listIdRicks[i];
                mainList.add(backListInBase[i]);
            }
        } else {
            for(Integer i=0; i<listMorty.size(); i++) {
                backListInBase[i].MyRick__c = listIdRicks[i];
                mainList.add(backListInBase[i]);
            }
        }
        update mainList;    //update Morty with fill field MyRick__c
    }
    */

    //nightmare
    /*
    public static void addRickForMorty(List<Contact> listCreatedContacts) {
        //listCreatedContacts - list create now (Morty and Rick, or only Morty)
        List<Contact> listMorty = new List<Contact>();
        List<Contact> listRick = new List<Contact>();
        for(Contact item : listCreatedContacts) {
            if(item.FirstName=='Morty') {       //
                listMorty.add(item);    //  take only Morty from incoming list from trigger
            } else {
                listRick.add(item);     //  take only Rick from incoming list from trigger
            }
        }
        List<Id> listIdMorty = new List<Id>();
        for(Contact item : listMorty) {
            listIdMorty.add(item.Id);
        }
        List<Contact> listWithMortyFromBase = [SELECT FirstName, Id, MyRick__c FROM Contact WHERE MyRick__c!=null];
        //listWithMortyFromBase - list from base with Morty with id Rick
        List<Contact> listWithMortyFromBaseFreeField = [SELECT FirstName, Id, MyRick__c FROM Contact WHERE MyRick__c=null AND Id IN : listIdMorty];
        //listWithMortyFromBase - list from base with Morty without id Rick
        List<Id> idWhatUsed = new List<Id>();
        for(Contact item : listWithMortyFromBase) {
            idWhatUsed.add(item.MyRick__c);     //idWhatUsed - list with used id Rick
        }
        List<Contact> listWithRickFromBase = [SELECT FirstName, Id FROM Contact WHERE Id NOT  IN : idWhatUsed AND FirstName='Rick'];
        //listWithRickFromBase - list from base with Ricks, all from base and id is free

        if(listWithRickFromBase.size() > listWithMortyFromBaseFreeField.size()) {
            for(Integer i=0; i<listWithMortyFromBaseFreeField.size(); i++) {
                listWithMortyFromBaseFreeField[i].MyRick__c = listWithRickFromBase[i].Id;
            }
        } else if(listWithRickFromBase.size() < listWithMortyFromBaseFreeField.size()) {
            for(Integer i=0; i<listWithRickFromBase.size(); i++) {
                listWithMortyFromBaseFreeField[i].MyRick__c = listWithRickFromBase[i].Id;
            }
        } else {
            for(Integer i=0; i<listWithMortyFromBaseFreeField.size(); i++) {
                listWithMortyFromBaseFreeField[i].MyRick__c = listWithRickFromBase[i].Id;
            }
        }
        update listWithMortyFromBaseFreeField;
    }
    */
}