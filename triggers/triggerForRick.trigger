/**
 * Created by User on 27.02.2019.
 */

trigger triggerForRick on Contact (before insert, after insert, before update) {
    //easy

    if(trigger.isBefore && trigger.isInsert) {
        handleForRick.addRickForMorty(Trigger.new);
    }


    //medium
    /*
    if(trigger.isAfter && trigger.isInsert) {
        handleForRick.addIDRickForMorty(Trigger.new);
    }
    if((trigger.isBefore && trigger.isInsert) || (trigger.isBefore && trigger.isUpdate)){
        handleForRick.checkBox(Trigger.new);
    }
    */

    //hard
    /*
    if((trigger.isBefore && trigger.isInsert) || (trigger.isBefore && trigger.isUpdate)){
        handleForRick.checkBox(Trigger.new);
    }

    if(trigger.isAfter && trigger.isInsert) {
        handleForRick.addRickToMorty(Trigger.new);
    }
    */

    //nightmare
    /*
    if(trigger.isAfter && trigger.isInsert) {
        handleForRick.addRickForMorty(Trigger.new);
    }
    */
}